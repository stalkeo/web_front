<%@page contentType="text/html" pageEncoding="UTF-8" language="java" autoFlush="true" buffer="12kb"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
        <title>El badulaque</title>

        <!-- CSS files -->
        <link href='style/style-perfil.css' rel='stylesheet'>
        
        <!-- Favicon -->
        <link rel='icon' href='img/Wallapop.ico'>
        		
    </head>
    <body>
    
    
<!-- HEADER ---------------------------------------------------------------------------------------------------------------- -->   
 		<%@ include file= "header2.html" %>
<!-------------------------------------------------------------------------------------------------------------------------- -->

		<!-- CUERPO -->
		<div id = "medium">

    		 <jsp:useBean id="datos_user" class="g8107.Beans.UserBean"/>
    		 <jsp:setProperty property="e_mail" name="datos_user" value = "<%=session.getAttribute("e_mail")%>"/>
		
			<div id = "head">
				<div id = "imagen_perfil">
					<span id = "texto_imagen_perfil">Bienvenido a tu perfil, <jsp:getProperty property="e_mail" name="datos_user"/> </span>
				</div>		
			</div>
			
			<div id = "body_impostor">
			
				<div id = "opciones">			
					<div id = "menu_opciones">
						<button class = "button" type="button" onclick = "mostrarP(3)">Mis datos</button> 
						<button class = "button" type="button" onclick = "mostrarP(1)">Mis productos</button> 
						<button class = "button" type="button" onclick = "mostrarP(4)">Mis chats</button>
						<form method = "POST" action = "ControlServlet">
							<div><input type="hidden" value="6" name="n_peticion" /></div> 
							<input type ="submit" value ="Cerrar sesión" class = "button" type="button" style>
						</form>
					</div>
				</div>		
				
				<div id = "mis_datos">			
					<label style = "font-size: 25px;">Mis datos</label>
				
					<div id = "contenedor_datos">
						<form method = "POST" action = "ControlServlet">
							<div><input type="hidden" value="7" name="n_peticion" /></div>
							<div class = "columna_datos">
								Nombre
								<input type="text" id="fname" name="nombre" value =<jsp:getProperty property="nombre" name="datos_user"/>>
								Primer apellido
								<input type="text" id="fname" name="apellido1" value = <jsp:getProperty property="apellido1" name="datos_user"/>>
								Segundo apellido
								<input type="text" id="fname" name="apellido2" value = <jsp:getProperty property="apellido2" name="datos_user"/>>
								Codigo Postal
								<input type='number' name = "cod_postal" id = "cod_postal" value = <jsp:getProperty property="codPostal" name="datos_user"/>>
							
							</div>
							
							<div class = "columna_datos">
								Contraseña Actual
								<input type="password" id="fname" name="pass_old"><br>
								Nueva contraseña
								<input type="password" id="fname" name="pass1"><br>
								Confirmar contraseña
								<input type="password" id="fname" name="pass2"><br>
								Ciudad de residencia
								<select name= "provincia" id = "provinciaID" value=<jsp:getProperty property="ciudad" name="datos_user"/>>
									 <option value=<jsp:getProperty property="ciudad" name="datos_user"/>><jsp:getProperty property="ciudad" name="datos_user"/></option>
									 <option value='Alava'>Alava</option>
									 <option value='Albacete'>Albacete</option>
									 <option value='Alicante'>Alicante</option>
									 <option value='Almeria'>Almeria</option>
									 <option value='Asturias'>Asturias</option>
									 <option value='Avila'>Avila</option>
									 <option value='Badajoz'>Badajoz</option>
									 <option value='Barcelona'>Barcelona</option>
									 <option value='Burgos'>Burgos</option>
									 <option value='Caceres'>Caceres</option>
									 <option value='Cadiz'>Cadiz</option>
									 <option value='Cantabria'>Cantabria</option>
									 <option value='Castellonn'>Castellon</option>
									 <option value='Ceuta'>Ceuta</option>
									 <option value='Ciudad Real'>Ciudad Real</option>
									 <option value='Cordoba'>Cordoba</option>
									 <option value='Cuenca'>Cuenca</option>
									 <option value='Girona'>Girona</option>
									 <option value='Las Palmas'>Las Palmas</option>
									 <option value='Granada'>Granada</option>
									 <option value='Guadalajara'>Guadalajara</option>
									 <option value='Guipuzcoa'>Guipuzcoa</option>
									 <option value='Huelva'>Huelva</option>
									 <option value='Huesca'>Huesca</option>
									 <option value='Islas Baleares'>Islas Baleares</option>
									 <option value='Jaen'>Jaen</option>
									 <option value='A Coruna'>A Coruna</option>
									 <option value='La Rioja'>La Rioja</option>
									 <option value='Leon'>Leon</option>
									 <option value='Lleida'>Lleida</option>
									 <option value='Lugo'>Lugo</option>
									 <option value='Madrid'>Madrid</option>
									 <option value='Malaga'>Malaga</option>
									 <option value='Melilla'>Melilla</option>
									 <option value='Murcia'>Murcia</option>
									 <option value='Navarra'>Navarra</option>
									 <option value='Ourense'>Ourense</option>
									 <option value='Palencia'>Palencia</option>
									 <option value='Pontevedra'>Pontevedra</option>
									 <option value='Salamanca'>Salamanca</option>
									 <option value='Segovia'>Segovia</option>
									 <option value='Sevilla'>Sevilla</option>
									 <option value='Soria'>Soria</option>
									 <option value='Tarragona'>Tarragona</option>
									 <option value='Santa Cruz Tenerife'>Santa Cruz de Tenerife</option>
									 <option value='Teruel'>Teruel</option>
									 <option value='Toledo'>Toledo</option>
									 <option value='Valencia'>Valencia</option>
									 <option value='Valladolid'>Valladolid</option>
									 <option value='Vizcaya'>Vizcaya</option>
									 <option value='Zamora'>Zamora</option>
									 <option value='Zaragoza'>Zaragoza</option>
								</select></div>
								
								<input type= "submit" value = "Actualizar" class = "button" type="button" style = "width: 100px; margin-top: 0; margin-left: 8%; font-size: 13px; padding: 0; background-color: #80c2e4;text-align:center;margin-bottom:2%" > 
								<button class = "button" type="button" onclick = "mostrarP(2)" style = "width: 100px; margin-top: 0; margin-right: 8%; font-size: 13px; padding: 0; background-color: #e95f5a;float:right" >Dar de baja</button> 
							</form>
					</div>
					
				</div>
				
				<div id = "mis_productos">
					<div id=productos>
					
						
					
						<c:forEach items = "${requestScope.productosUsuario}" var = "item" varStatus = "status">

							<div class="product">
							
								<form method = "POST" action = "ControlServlet">
									<div><input type="hidden" value="9" name="n_peticion" /></div>
									
									<!--<div class="imagen-principal"><a href="#Producto2"><img src= "${requestScope.rutas[status.index]}"></a></div>--> 	
									<div class ="imagen-principal"><input type = "image" name = "submit" src = "${requestScope.rutas[status.index]}"></div>
																												
									<input type = "hidden" name = "precioPerfil" id = "precioPerfilID" value = "${item.getPrecio()}">
									<div class=price>${item.getPrecio()}€</div>
									
									<input type = "hidden" name = "nombrePerfil" id = "nombrePerfilID" value = "${item.getTitulo()}">	
									<div class=name><a>${item.getTitulo()}</a></div>
									
									<input type = "hidden" name = "categoriaPerfil" id = "categoriaPerfilID" value = "${item.getCategoria()}">	
									<div class=type><a>${item.getCategoria()}</a></div>
									
									<input type = "hidden" name = "usuarioPerfil" id = "usuarioPerfilID" value ="${item.getUsuario().getNombre()} ${item.getUsuario().getApellido1()}" >
									<div class=product-user><a>${item.getUsuario().getNombre()} ${item.getUsuario().getApellido1()}</a></div>
															
									<div><input type="hidden" value="${item.getId()}" name="id_oculto" /></div>
									
								</form>
							</div>

						</c:forEach>
					
						
					<!--<div class="product">
							<div class="imagen-principal"><a href="#Producto2"><img src="img/producto1.jpg"></img></a></div>
							<div class=price>200€</div>
							<div class=name><a href="#Pag-Product">Intel core i7 4790k</a></div>
							<div class=type><a href="#Electrónica">Electrónica</a></div>
							<div class=product-user><a href="#Pag-user">Lirico</a></div>
						</div>
						
						<div class="product">
							<div class="imagen-principal"><a href="#Producto2"><img src="img/producto1.jpg"></img></a></div>
							<div class=price>200€</div>
							<div class=name><a href="#Pag-Product">Intel core i7 4790k</a></div>
							<div class=type><a href="#Electrónica">Electrónica</a></div>
							<div class=product-user><a href="#Pag-user">Lirico</a></div>
						</div>
						
						<div class="product">
							<div class="imagen-principal"><a href="#Producto2"><img src="img/producto1.jpg"></img></a></div>
							<div class=price>200€</div>
							<div class=name><a href="#Pag-Product">Intel core i7 4790k</a></div>
							<div class=type><a href="#Electrónica">Electrónica</a></div>
							<div class=product-user><a href="#Pag-user">Lirico</a></div>
						</div>
						
						<div class="product">
							<div class="imagen-principal"><a href="#Producto2"><img src="img/producto1.jpg"></img></a></div>
							<div class=price>200€</div>
							<div class=name><a href="#Pag-Product">Intel core i7 4790k</a></div>
							<div class=type><a href="#Electrónica">Electrónica</a></div>
							<div class=product-user><a href="#Pag-user">Lirico</a></div>
						</div>-->
						
					</div>
				</div>	
				
				<div id = "chats">
					<label style = "font-size: 25px;">Chats</label>
				
					<c:set var="con_chats" value= "${1}"/>
						<c:if test="${requestScope.mensajes != null}">
								<c:forEach items="${requestScope.mensajes}" var="item">
								<div id = "contenedor_datos">
									<h3 style="padding-top: 1%;padding-bottom: 1%;margin-top: 0%;margin-bottom: 0%;margin-left: 2%">Chat: ${con_chats}</h3>
										<div style="margin-left: 2%;">${item}</div><br>
								</div>
							<c:set var="con_chats" value= "${con_chats + 1}"/>
							</c:forEach>
					    </c:if>
				    
					<form method = "POST" action = "ControlServlet">
						<div><input type="hidden" value="12" name="n_peticion" /></div> 
						<div><input type="hidden" value="1" name="accion_chat" /></div> 
						<input type ="submit" value ="Leer todos mis mensajes" class = "button" type="button" style="margin-top: 1%;width:100%">
					</form>
		
		
					<form method = "POST" action = "ControlServlet">
						<div id = "contenedor_datos" style="padding-top: 1%;" style="padding-top: 1%;">
							<div style="height: 40px;margin-left: 10%;"><input type="hidden" value="12" name="n_peticion" /></div> 
							<div><input type="hidden" value="2" name="accion_chat" /></div>
							<div style="height: 40px;margin-left: 10%;"><label>Destinatario:</label><input type="text" name="destinatario" style="margin-left: 20px;width: 60%;"/></div>
							<div style="height: 170px;margin-left: 10%;"><label>Mensaje:</label><textarea type="text" name="new_mensaje" style="margin-left: 53px;width: 60%;height: 150px;"></textarea></div>
						</div>
						<input type ="submit" value ="Escribir" class = "button" type="button" style="margin-top: 1%;;width:100%"">
					</form>	
				</div>
				
				<c:if test="${requestScope.observando_chats == 1}">
			    	<script>		
				    	document.getElementById("mis_productos").style.display = "none";
						document.getElementById("mis_datos").style.display = "none";
						document.getElementById("chats").style.display = "inline";
					</script>
				</c:if>	
					
			</div>					

		</div>
		
	<div class = "fondo_modal" onclick = "OcultarP()">
	   <div class = "formulario" onclick = "cancelarP()">
	     <form method = "POST" action = "ControlServlet" class='contacto' onsubmit="return comprobarPassword()">
	       <div id = "Titulo_logeo">Confirmación de baja </div>
	       <div><input type="hidden" value="8" name="n_peticion" /></div>
	       <div><label>Contrase&#241a:</label><input type='password' name = "contrasena" id = "pass" value = '' required style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
	       <div style = "display:none"><label>Peticion</label> <input type = "hidden" name = "peticion" value = "Baja"></div>
	       <div><center><input type = 'submit' value = 'Confirmar' style = "font-family: sans-serif; font-size: 12px; color: #798e94;"></center></div>
	     </form>
	   </div> 
	 </div>
		
		
<!-- FOOTER --------------------------------------------------------------------------------------------------------------- -->
		<%@ include file= "footer.html" %>
<!------------------------------------------------------------------------------------------------------------------------- -->		

		<!-- SCRIPT -->
		<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="script/perfil.js"></script>
		
    </body>
</html>