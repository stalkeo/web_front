# README #

### ¿Para qué sirve este repositorio? ###

* Desarrolla el front de la página web de una versión de Wallapop, destinada a la realización de la práctica de la asignatura de Tecnologías Informáticas para la Web.


### ¿Cómo lo pongo en marcha? ###

1. Incluir el contenido de este repositorio en la carpeta WebContent de un Maven Proyect, en Eclipse.
2. Lanzar esta aplicación en un servidor.
3. Abrir en el navegador la página de index.


### ¿Con quién hablo? ###

#### Componentes del equipo de desarrollo ####

* Luis Alberto Pantaleón del Puerto
* Juan Alonso Machuca González
* Jose Luis Parra Olmedilla
* Mario Sánchez García

* Universidad Carlos III  de Madrid (Campus de Leganés)