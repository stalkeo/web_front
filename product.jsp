<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" autoFlush="true" buffer="12kb"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>El badulaque</title>
        
        <!-- CSS Product -->
        <link href="style/style-product.css" rel="stylesheet">
        
        <!-- Favicon -->
        <link rel="icon" href="img/Wallapop.ico">
        
	</head>
	
	<body>
	
	<!-- HEADER ---------------------------------------------------------------------------------------------------------------- -->
		<c:if test="${!empty sessionScope.e_mail}">
    		<%@ include file= "header2.html" %>
		</c:if>
		
		<c:if test="${empty sessionScope.e_mail}">
			<%@ include file= "header.html" %>
		</c:if>
	<!-------------------------------------------------------------------------------------------------------------------------- -->
	
	<!-- CUERPO ---------------------------------------------------------------------------------------------------------------- -->
		
		<div id=medium>
			<div class='wrap-2'>
				<div class='imgs-container'>
					
					<c:if test = "${requestScope.numImgR >0}">
					<div class='img_ppal'> <img src="${requestScope.rutasR.get(0)}"></div>
					</c:if>
					<div class='caroussel'>
						<c:if test = "${requestScope.numImgR >1}">
						<div class='img_sec'> <img src="${requestScope.rutasR.get(1)}"> </div>
						</c:if>
						<c:if test = "${requestScope.numImgR >2}">
						<div class='img_sec'> <img src="${requestScope.rutasR.get(2)}"> </div>
						</c:if>
						<c:if test = "${requestScope.numImgR >3}">
						<div class='img_sec'> <img src="${requestScope.rutasR.get(3)}"> </div>
						</c:if>
					</div>	
					
				</div>

				<div class='data-container'>
					<div class='data'>
						<p id='precio'>${requestScope.precioPerfil}€</p>
							<div class='border_impostor'></div>
						<h3 id='titulo'>${requestScope.nombrePerfil}</h3>
							<div class='border_impostor'></div>
						<p id='descripcion'>${requestScope.descripcionPerfil} </p>
						
							<div class='border_impostor'></div>
							
						<c:if test = "${requestScope.estadoPerfil == 0}">
							<button id = "button5" type="button" style = "cursor: default; margin: 16px 0; width: 100px; font-size: 13px; padding: 0; background-color: #40f978;text-align:center; float: left;">Disponible</button>
						</c:if>						
						<c:if test = "${requestScope.estadoPerfil == 1}">
							<button id = "button5" type="button" style = "cursor: default; margin: 16px 0; width: 100px; font-size: 13px; padding: 0; background-color: #eec719;text-align:center; float: left;">Reservado</button>
						</c:if>						
						<c:if test = "${requestScope.estadoPerfil == 2}">
							<button id = "button5" type="button" style = "cursor: default; margin: 16px 0; width: 100px; font-size: 13px; padding: 0; background-color: #e95911;text-align:center; float: left;">Vendido</button>
						</c:if>	
						
							<div class='border_impostor'></div>
							
						<p id = 'categoria'>${requestScope.categoriaPerfil} </p>
						<div class='border_impostor'></div>
						<p id='cp'>${requestScope.codPostal}, ${requestScope.ciudad}</p>
					</div>
					<div id='data-user' class='data'>
						<p> ${requestScope.usuarioPerfil} </p>
						<p> ${requestScope.emailPerfil} </p>
						<c:if test = "${requestScope.user_visitor == requestScope.user_owner}">
							<div class='border_impostor'></div>
							<div id = "editar_estado">					
								<button id = "button5" type="button" onclick = "mostrarPD(1)" style = "cursor: pointer; width: 100px; margin: 16px 20px; font-size: 13px; padding: 0; background-color: #80c2e4; text-align:center; float: left; margin-left: 0px;" >Editar producto</button>
								<button id = "button5" type="button" onclick = "mostrarPD(2)" style = "cursor: pointer; width: 100px; margin: 16px 20px; font-size: 13px; padding: 0; background-color: #e95f5a; text-align:center; float: right;" >Dar de baja</button>
							</div>
						</c:if>	
					</div>
					
					
					
						

			</div>
		
		</div>
		<c:if test = "${requestScope.user_visitor == requestScope.user_owner}">
		<div class = "fondo_modal" onclick = "OcultarPD()">
		   <div class = "formulario1" onclick = "cancelarPD()">
		     <form enctype="multipart/form-data" method = "POST" action = "ControlServlet" class='contacto' onsubmit="return comprobarAltaProducto()">
		       <div id = "Titulo_logeo">Editar el producto </div>
		       <div><input type="hidden" value="11" name="n_peticion" /></div>
		       <div><label>Nombre del producto:</label><input type='text' name = "nombreP" id = "nombrePID" required value = "${requestScope.nombrePerfil}" style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
			   <div><label style = "float: left;">Im&#225;gene principal del producto:</label><input type = 'file' accept = "image/*" name ="imagenP1O" id = "imagenPID1" style ="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0; float: left;"></div>
			   <div><label style = "float: left;">Im&#225;genen opcional 1:</label><input type = 'file' accept = "image/*" name ="imagenP2O" id = "imagenPID2" style ="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0; float: left;"></div>
			   <div><label style = "float: left;">Im&#225;genen opcional 2:</label><input type = 'file' accept = "image/*" name ="imagenP3O" id = "imagenPID3" style ="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0; float: left;"></div>
			   <div><label style = "float: left;">Im&#225;genen opcional 3:</label><input type = 'file' accept = "image/*" name ="imagenP4O" id = "imagenPID4" style ="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0; float: left;"></div>
			   <div style = "float:left; font-weight: bold; margin-top: 25px;">Categor&#237;a</div>
			   <div style = "float: right; margin-top: 20px;">
					<select name="category" id = "category" style="height:25px; width: 150px;">
						<option value= '${requestScope.categoriaPerfil}' >${requestScope.categoriaPerfil}</option>
						<option value='Todo'>Todo</option>
			                    <option value='Tecnologia e Informatica'>Tecnologia e Informatica</option>
			                    <option value='Coches y Motos'>Coches y Motos</option>
			                    <option value='Deporte y Ocio'>Deporte y Ocio</option>
			                    <option value='Muebles, Deco y Jardin'>Muebles, Deco y Jardin</option>
			                    <option value='Consolas y Videojuegos'>Consolas y Videojuegos</option>
			                    <option value='Libros, Peliculas y Musica'>Libros, Peliculas y Musica</option>
			                    <option value='Moda y Accesorios'>Moda y Accesorios</option>
			                    <option value='Juguetes, Ninos y Bebes'>Juguetes, Ninos y Bebidas</option>
			                    <option value='Inmobiliaria'>Inmobiliaria</option>
			                    <option value='Electrodomesticos'>Electrodomesticos</option>
			                    <option value='Servicios'>Servicios</option>
			                    <option value='Otros'>Otros</option>
					</select>
				</div>
				<div><label style = "float: left; margin-top: 15px;">Descripci&#243;n del producto</label><textarea name="Comentarios" cols="30" rows="3" maxlength="500" id = "comentario1" required>${requestScope.descripcionPerfil}</textarea></div>
				<div style = "float:left; font-weight: bold; margin-top: 8px;">Precio (&#8364;)</div>
				<div style = "float: right;"><input type = 'number' name = "precioP" id = "precioPID" required value = "${requestScope.precioPerfil}" style = "height:25px; width: 150px;"></div>
				<div style = "float:left; font-weight: bold; margin-top: 25px; clear: left;">Estado</div>
			   <div style = "float: right; margin-top: 10px;">
					<select name="estado" id = "estado" style="height:25px; width: 156px;">
						<option value='0'>Disponible</option>
						<option value='1'>Reservado</option>
						<option value='2'>Vendido</option>
					</select>
				</div>
				<div><input type="hidden" value="${requestScope.id}" name="id_oculto" /></div>
				<div><input type="hidden" value="${requestScope.ciudad}" name="provincia_oculta" /></div>
				<div><input type="hidden" value="${requestScope.codPostal}" name="cod_postal_oculta" /></div>
				<div style = "clear: left;"><input type = 'submit' value = 'Modificar' style = "margin-top: 5px; margin-bottom: 20px; font-family: sans-serif; font-size: 12px; color: #798e94; float: left;"></div>
		     </form>
		   </div> 
		 </div>
		 </c:if>
		
		
	<c:if test = "${requestScope.user_visitor == requestScope.user_owner}">
		<div class = "fondo_modal" onclick = "OcultarPD()">
      	 <div class = "formulario" onclick = "cancelarPD()">
         <form method = "POST" action = "ControlServlet" class='contacto'">
           <div id = "Titulo_logeo"  style="font-size: 19px">Confirmación baja producto</div>
           <div><input type="hidden" value="14" name="n_peticion" /></div>
           <div><input type="hidden" value="${requestScope.id}" name="id_oculto" /></div>
           <div><center><input type = 'submit' value = 'Confirmar' style = "font-family: sans-serif; font-size: 12px; color: #798e94;"></center></div>
         </form>
       </div>
    </div>
	</c:if>
	<!-------------------------------------------------------------------------------------------------------------------------- -->
	
	<!-- FOOTER --------------------------------------------------------------------------------------------------------------- -->
		<%@ include file= "footer.html" %>
	<!------------------------------------------------------------------------------------------------------------------------- -->
	
		<!-- SCRIPT -->
		<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>	
		<script type="text/javascript" src="script/product.js"></script>
		
	</body>
</html>