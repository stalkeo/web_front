var ocultar;

$(document).ready( function () {

	var flag_list_visible = false;
	var flag_adv_search_hidden = true;

	$(document).mouseup(function (click_) {		
	    var list = $(".menu-list");
	    var list_p = $(".menu-list_pico");
	    var button = $(".menu-button");

	    var adv_search = $('#adv_search');
	    var adv_search_button = $('.adv_from_top');

	    if (!list.is(click_.target) && list.has(click_.target).length === 0
	    		&& !list_p.is(click_.target) && list_p.has(click_.target).length === 0
	    		&& !button.is(click_.target) && button.has(click_.target).length === 0) {
	        list.css('display', 'none');
	    	list_p.css('display', 'none');
	        flag_list_visible = false;
	    }

	    if (!adv_search.is(click_.target) && adv_search.has(click_.target).length === 0
		    	&& !adv_search_button.is(click_.target) && adv_search_button.has(click_.target).length === 0) {
			console.log('desaparece');
			adv_search.slideUp();
			flag_adv_search_hidden = true;	
	    }
	    
	    

	});

	$('.adv_from_top').click( function () {
		var adv_search = $('#adv_search');

		if (flag_adv_search_hidden) {
			console.log('aparece');
			adv_search.slideDown();
			flag_adv_search_hidden = false;
		}
		else {
			console.log('desaparece');
			adv_search.slideUp();
			flag_adv_search_hidden = true;
		}

	});


	$('.menu-button').click( function () {

		//console.log ('BUTTON----------------');
		//console.log ('before ' + flag_list_visible);

		if (flag_list_visible) {
			$(".menu-list_pico").css('display', 'none');
			$(".menu-list").css('display', 'none');
			flag_list_visible = false;
		}			
		else {
			$(".menu-list_pico").css('display', 'block');
			$(".menu-list").css('display', 'block');
			flag_list_visible = true;
		}

		//console.log ('after ' + flag_list_visible + '\n');
	});

	$('.menu-list').hover(
		function mouse_in () {
			var button = $('.menu-button');

			button.css('box-shadow', '0 2px 5px #666666');
			button.css('background-color', 'gray');
		},
		function mouse_out () {
			var button = $('.menu-button');

			button.css('box-shadow', '0 0 0');
			button.css('background-color', 'transparent');
		});

	$('.menu-button').hover(
		function mouse_in () {
			$(this).css('box-shadow', '0 2px 5px #666666');
			$(this).css('background-color', 'gray');
		},
		function mouse_out () {
			$(this).css('box-shadow', '0 0 0');
			$(this).css('background-color', 'transparent');
		});



});


var ocultar;

function mostrar(){
	
	
	var modal = document.getElementsByClassName("fondo_modal")[0].style.display = "flex";
	
	ocultar = true;
}


function Ocultar(x){

	
	if (ocultar){
		
		var modal = document.getElementsByClassName("fondo_modal");
		
		modal[0].style.display = "none";

	}
	
	else {
		
		ocultar = true;
	}
}

function cancelar(){

	ocultar = false;
}

function comprobarAltaProducto(){
		
	var inp = document.getElementById('imagenPID');
	
	if (inp.files.length > 4){
		
		alert ("El n\u00FAmero m\u00E1ximo de im\u00E1genes es 4");
		return false;
	}
	
	return true;

	
}
