<%@page contentType="text/html" pageEncoding="UTF-8" language="java" autoFlush="true" buffer="12kb"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>El badulaque</title>

        <!-- CSS Results -->
        <link href="style/style-results.css" rel="stylesheet">
        
        <!-- Favicon -->
        <link rel="icon" href="img/Wallapop.ico">

    </head>
    <body>
	
<!-- HEADER ---------------------------------------------------------------------------------------------------------------- -->
		<c:if test="${!empty sessionScope.e_mail}">
    		<%@ include file= "header2.html" %>
		</c:if>
		
		<c:if test="${empty sessionScope.e_mail}">
			<%@ include file= "header.html" %>
		</c:if>
<!-------------------------------------------------------------------------------------------------------------------------- -->

	<div id='medium'>
		<div id='productos'>

	<c:forEach items="${requestScope.adv_search_res}" var="item" varStatus="status">
	
		<div class="product">							
			<form method = "POST" action = "ControlServlet">
				<div><input type="hidden" value="9" name="n_peticion" /></div>
				
				<div class="imagen-principal"><input type = "image" name = "submit" src = "${requestScope.adv_search_img_path[status.index]}"></div>
				<input type = "hidden" name = "precioPerfil" id = "precioPerfilID" value = "${item.getPrecio()}">
				<div class="price">${item.getPrecio()}€</div>
				
				<input type = "hidden" name = "nombrePerfil" id = "nombrePerfilID" value = "${item.getTitulo()}">
				<div class="name"><a>${item.getTitulo()}</a></div>
				
				<input type = "hidden" name = "categoriaPerfil" id = "categoriaPerfilID" value = "${item.getCategoria()}">
				<div class="type"><a>${item.getCategoria()}</a></div>
				
				<input type = "hidden" name = "usuarioPerfil" id = "usuarioPerfilID" value ="${item.getUsuario().getNombre()} ${item.getUsuario().getApellido1()}" >
				<div class=product-user><a>${item.getUsuario().getNombre()} ${item.getUsuario().getApellido1()}</a></div>
										
				<div><input type="hidden" value="${item.getId()}" name="id_oculto" /></div>
				
			</form>
		</div>
		
	</c:forEach>		

		</div>
	</div>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------- -->
		<%@ include file= "footer.html" %>
<!------------------------------------------------------------------------------------------------------------------------- -->		
			
	<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>

    </body>
</html>
