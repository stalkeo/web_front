<%@page contentType="text/html" pageEncoding="UTF-8" language="java" autoFlush="true" buffer="12kb"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>404 Error</title>

        <!-- CSS Index -->
        <link href="style/style-err.css" rel="stylesheet">
        
        <!-- Favicon -->
        <link rel="icon" href="img/Wallapop.ico">

    </head>
    <body>
	
<!-- HEADER ---------------------------------------------------------------------------------------------------------------- -->
		<c:if test="${!empty sessionScope.e_mail}">
    		<%@ include file= "header2.html" %>
		</c:if>
		
		<c:if test="${empty sessionScope.e_mail}">
			<%@ include file= "header.html" %>
		</c:if>
<!-------------------------------------------------------------------------------------------------------------------------- -->

		<section id='content'>
			<div class='content-wrap'>
				<h1>Lo sentimos</h1>
				<h3>La página que busca no existe o no est&#225; disponible. Puede volver a la p&#225;gina de inicio haciendo click <a href='index.jsp'>aqu&#237;</a>.</h3>
			</div>
		</section>

<!-- FOOTER --------------------------------------------------------------------------------------------------------------- -->
		<%@ include file= "footer.html" %>
<!------------------------------------------------------------------------------------------------------------------------- -->		
			
	<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>

    </body>
</html>
